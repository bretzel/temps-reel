# Horaires du tramway de Strasbourg

## Prérequis

* [Apache](https://httpd.apache.org/) ou [NGINX](http://nginx.org/)
* [Node.js](https://nodejs.org/fr/)
* [npm](https://www.npmjs.com/)
* [Un token de la CTS](https://www.cts-strasbourg.eu/fr/portail-open-data/)

## Installation

Installation sur Debian 10 avec NGINX

```
sudo apt install nginx nodejs npm express
git clone https://gitlab.com/NathanS67/temps-reel.git
# N'oubliez pas de configurer nginx pour que le répertoire racine soit "temps-reel".
cd temps-reel/server/
npm install node-fetch btoa express fs
echo "VOTRE_TOKEN">TOKEN
# Ce script permet de générer les données en temps réel, il est nécessaire de le laisser tourner en continu.
# Pour l'éxecuter en arrière plan en enregistrant les logs.
node generation.js >> log.txt 2>&1 &
```

## Informations supplémentaires

* Pour utiliser la géolocalisation, il est nécessaire que le site dispose d'une couche de chiffrement (HTTPS). Pour obtenir un certificat TLS gratuitement, renseignez-vous auprès de [Let's Encrypt](https://letsencrypt.org/fr/)

## Captures d'écran
<details><summary>Pour voir les captures d'écran, cliquez ici.</summary>
![alt text](https://imgur.com/4kRPBV9.png)
![alt text](https://imgur.com/gHFGm5R.png)
</details>

## Contributeurs

* **Nathan Spaeter** - *Créateur* - [NathanS67](https://gitlab.com/NathanS67)

## License

Ce projet est sous licence GNU GPLv3. Voir le fichier [LICENSE.md](https://gitlab.com/NathanS67/temps-reel/blob/master/LICENSE) pour plus de détails

## Remerciements

Merci à la [Compagnie des transports strasbourgeois (CTS)](https://www.cts-strasbourg.eu/fr/) de mettre à disposition ses données, notamment tout ce qui est relatif au tramway de Strasbourg.

## Liste de choses à faire

* [ ]   Ajouter un algorithme de plus court chemin pour faire un itinéraire (Algorithme de Dijkstra).
* [ ]   Ajouter un système de tri pour par exemple n'afficher sur un arrêt uniquement une destination.
* [ ]   Corriger le problème qui crée une exception quand plus aucun tram ne circule sur le réseau.
* [ ]   Afficher dans la liste l'ensemble des arrêts et désactiver (rendre impossible la sélection) les arrêts dont plus aucun tram ne circule dessus (travaux, nuit, ...)
* [ ]   Ajouter en plus des arrêts en temps réels, les arrêts théoriques afin d'avoir davantage d'arrêt.
* [ ]   Afficher correctement les lignes sur le plan.