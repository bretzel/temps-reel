async function importer(url) {
    try {
        const response = await fetch(url, { method: "GET" });
        const data = await response.json();
        return data;
    } catch (err) {
        console.error(err);
    }
}

function carte() {
    let map = L.map('map', {
        minZoom: 10,
        maxZoom: 19
    });
    map.setView([48.584293883712505, 7.7447360381375], 15);
    L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
        subdomains: 'abcd',
        maxZoom: 19
    }).addTo(map);

    return map;
}

let arrets = importer(location.href + "/arrets.geojson");
let rails = importer(location.href + "/rails.geojson");

let carto = carte();

Promise.all([arrets, rails]).then(([arrets, rails]) => {
    L.geoJSON(arrets).bindPopup(function (layer) {
        return layer.feature.properties.name;
    }).addTo(carto);

    L.geoJSON(rails).bindPopup(function (layer) {
        return layer.feature.properties.name;
    }).addTo(carto);
});