const stop = () => {
    try {
        while (document.querySelector("tbody").firstChild) {
            document.querySelector("tbody").removeChild(document.querySelector("tbody").firstChild)
        }
        for (let i = 0; i < temps_reel.length; i++) {
            if (num == temps_reel[i].numero) {
                let ligne = temps_reel[i].ligne;
                let terminus = temps_reel[i].terminus;
                let heure_depart = new Date(temps_reel[i].heure_depart);
                let h = new Date();
                h.setSeconds(0);
                h.setMilliseconds(0);
                let temps = Math.floor((heure_depart.getTime() - h.getTime()) / 1000 / 60);
                if (temps >= 0) {
                    if (temps == 0) {
                        temps = "Immédiatement"
                    } else {
                        if (temps == 1) {
                            temps += " minute"
                        } else {
                            temps += " minutes"
                        }
                    }

                    let tr = document.createElement("tr");

                    let span = document.createElement("span");
                    span.setAttribute("id", ligne);
                    span.setAttribute("class", "ligne");
                    span.appendChild(document.createTextNode(ligne));
                    let td2 = document.createElement("td");
                    td2.appendChild(span);
                    td2.appendChild(document.createTextNode(terminus));
                    tr.appendChild(td2);
                    let td3 = document.createElement("td");
                    td3.appendChild(document.createTextNode(temps_reel[i].heure_depart.slice(11, -9)));
                    tr.appendChild(td3);
                    let td4 = document.createElement("td");
                    td4.appendChild(document.createTextNode(temps));
                    tr.appendChild(td4);

                    document.querySelector("table tbody").appendChild(tr);
                }
            }
        }

    } catch (error) {
        console.error(error);
    }
}

const liste = (num) => {
    let numero;
    if (num == "") {
        numero = document.getElementById("liste").options[(document.getElementById("liste").selectedIndex)].value;
    } else {
        numero = num;
    }
    let url_ = new URL(document.URL);
    url_.searchParams.set("arret", numero);
    window.location = url_.href;
}

const arretProche = () => {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(geolocArret);
    } else {
        alert("La géolocalisation n\"est pas supportée par le navigateur.");
    }
}

const geolocArret = (position) => {
    let pos = [];
    for (let i = 0; i < tab_arrets.length; i++) {
        let lat = Math.abs(tab_arrets[i].latitude - position.coords.latitude);
        let long = Math.abs(tab_arrets[i].longitude - position.coords.longitude);
        pos.push({
            id: tab_arrets[i].id,
            nom: tab_arrets[i].nom,
            pos: lat + long
        });
    }
    pos.sort(compare);
    liste(pos[0].id);
}

const compare = (a, b) => {
    if (a.pos < b.pos) {
        return -1;
    }
    if (a.pos > b.pos) {
        return 1;
    }
    return 0;
}

const importer = async(url) => {
    try {
        const response = await fetch(url, { method: "GET" });
        const data = await response.json();
        return data;
    } catch (err) {
        console.error(err);
    }
}

const verif_heure = () => {
    heure1 = new Date();
    if (heure.getMinutes() != heure1.getMinutes()) {
        importer(url + "tempsreel/").then(data => {
            temps_reel = data;
            heure = heure1;
            stop();
        });
    }
    setTimeout(verif_heure, 1000);
}

let tab_arrets, temps_reel;
let url = "https://api.bretzel.ga/temps-reel/";

importer(url + "arrets/").then((tab) => {
    for (let i = 0; i < tab.length; i++) {
        let opt = document.createElement("option");
        opt.value = tab[i].id;
        opt.innerHTML = tab[i].nom;
        document.getElementById("liste").appendChild(opt);
        document.getElementById("liste").selectedIndex = 0;
    }
    document.getElementById("liste").selectedIndex = tab.findIndex(i => i.id == new URL(document.URL).search.slice(7)) + 1;
    tab_arrets = tab;
});
importer(url + "tempsreel/").then(data => {
    temps_reel = data;
    stop();
});
let num = new URL(document.URL).search.slice(7);
let heure = new Date();
verif_heure();
