const fetch = require("node-fetch");
const btoa = require("btoa");
const express = require("express");
const fs = require("fs");

const compare = (a, b) => {
    if (a.nom < b.nom) {
        return -1;
    }
    if (a.nom > b.nom) {
        return 1;
    }
    return 0;
}

const fetchCTS = (param) => {
    return fetch("https://api.cts-strasbourg.eu/v1/siri/2.0/" + param, {
            method: "GET",
            headers: {
                "Authorization": "Basic " + btoa(token + ":")
            }
        })
        .then(response => {
            return response.json()
        })
        .catch(err => {
            console.error("********** " + new Date().toString() + " **********\n\n" + err + "\n");
        });
}

const generation = () => {
    idArrets()
        .then((response) => {
            let data, id;
            [data, id] = response;
            if (id.length > 0) {
                requeteTempsReel(id).then((d) => vtempsreel = d);
                arrets(id).then((d) => {
                    if (d.length > varrets.length) { varrets = d }
                });
                vroutes = routes(data);
            }
        })
        .catch(err => {
            console.error("********** " + new Date().toString() + " **********\n\n" + err + "\n");
        });
    setTimeout(generation, 10000);
}

const requeteTempsReel = (tab) => {
    let ref = "";
    for (let i = 0; i < tab.length; i++) {
        ref += "MonitoringRef=" + tab[i] + "&";
    }
    return fetchCTS("stop-monitoring?" + ref + "VehicleMode=tram")
        .then(data => {
            let retour = [];
            let a = data.ServiceDelivery.StopMonitoringDelivery[0].MonitoredStopVisit;
            if (a) {
                for (i = 0; i < a.length; i++) {
                    retour.push({
                        numero: a[i].StopCode.slice(0, -1),
                        ligne: a[i].MonitoredVehicleJourney.PublishedLineName,
                        terminus: a[i].MonitoredVehicleJourney.DestinationName,
                        heure_depart: a[i].MonitoredVehicleJourney.MonitoredCall.ExpectedDepartureTime
                    })
                }
            }
            return retour;
        })
        .catch(err => {
            console.error("********** " + new Date().toString() + " **********\n\n" + err + "\n");
        })
}

const arrets = (tab) => {
    return fetchCTS("stoppoints-discovery")
        .then(data => {
            let tablArret = data.StopPointsDelivery.AnnotatedStopPointRef;
            let id = [];
            let retour = [];
            if (tablArret) {
                for (let i = 0; i < tablArret.length; i++) {
                    if (tab.includes(tablArret[i].Extension.LogicalStopCode)) {
                        if (!id.includes(tablArret[i].Extension.LogicalStopCode)) {
                            id.push(tablArret[i].Extension.LogicalStopCode);
                            retour.push({
                                id: tablArret[i].Extension.LogicalStopCode,
                                nom: tablArret[i].StopName,
                                longitude: tablArret[i].Location.Longitude,
                                latitude: tablArret[i].Location.Latitude
                            });
                        } else {
                            let a = id.indexOf(tablArret[i].Extension.LogicalStopCode);
                            let ret_id = retour[a].id;
                            let ret_nom = retour[a].nom;
                            let ret_long = (retour[a].longitude + tablArret[i].Location.Longitude) / 2;
                            let ret_lat = (retour[a].latitude + tablArret[i].Location.Latitude) / 2;
                            retour[a] = {
                                id: ret_id,
                                nom: ret_nom,
                                longitude: ret_long,
                                latitude: ret_lat
                            };
                        }
                    }
                }
            }
            retour.sort(compare);
            return retour;
        })
        .catch(err => {
            console.error("********** " + new Date().toString() + " **********\n\n" + err + "\n");
        });
}

const routes = (data) => {
    let retour = [];
    let i, j, k;
    let A = [],
        B = [],
        C = [],
        D = [],
        E = [],
        F = [];

    let lignes_tram = data.ServiceDelivery.EstimatedTimetableDelivery[0].EstimatedJourneyVersionFrame;
    if (lignes_tram) {
        for (i = 0; i < lignes_tram.length; i++) {
            let tram_spec = lignes_tram[i].EstimatedVehicleJourney;
            for (j = 0; j < tram_spec.length; j++) {
                let arret = tram_spec[j].EstimatedCalls;
                for (k = 0; k < arret.length; k++) {
                    switch (i) {
                        case 0:
                            if (!A.includes(arret[k].StopPointRef.slice(0, -1))) {
                                A.push(arret[k].StopPointRef.slice(0, -1));
                            }
                            break;
                        case 1:
                            if (!B.includes(arret[k].StopPointRef.slice(0, -1))) {
                                B.push(arret[k].StopPointRef.slice(0, -1));
                            }
                            break;
                        case 2:
                            if (!C.includes(arret[k].StopPointRef.slice(0, -1))) {
                                C.push(arret[k].StopPointRef.slice(0, -1));
                            }
                            break;
                        case 3:
                            if (!D.includes(arret[k].StopPointRef.slice(0, -1))) {
                                D.push(arret[k].StopPointRef.slice(0, -1));
                            }
                            break;
                        case 4:
                            if (!E.includes(arret[k].StopPointRef.slice(0, -1))) {
                                E.push(arret[k].StopPointRef.slice(0, -1));
                            }
                            break;
                        case 5:
                            if (!F.includes(arret[k].StopPointRef.slice(0, -1))) {
                                F.push(arret[k].StopPointRef.slice(0, -1));
                            }
                            break;
                    }
                }
            }
        }
    }
    retour.push(A, B, C, D, E, F);
    return retour;
}

const idArrets = () => {
    return fetchCTS("estimated-timetable?VehicleMode=tram&DirectionRef=0")
        .then(data => {
            let id = [];

            let i, j, k;
            let lignes_tram = data.ServiceDelivery.EstimatedTimetableDelivery[0].EstimatedJourneyVersionFrame;
            if (lignes_tram) {
                for (i = 0; i < lignes_tram.length; i++) {
                    let tram_spec = lignes_tram[i].EstimatedVehicleJourney;
                    for (j = 0; j < tram_spec.length; j++) {
                        let arret = tram_spec[j].EstimatedCalls;
                        for (k = 0; k < arret.length; k++) {
                            if (!id.includes(arret[k].StopPointRef.slice(0, -1))) {
                                id.push(arret[k].StopPointRef.slice(0, -1));
                            }
                        }
                    }
                }
            }
            return [data, id];
        })
        .catch(err => {
            console.error("********** " + new Date().toString() + " **********\n\n" + err + "\n");
        });
}

// Pour effectuer une demande : https://www.cts-strasbourg.eu/fr/portail-open-data/
let token = fs.readFileSync('./TOKEN', 'utf8');

let app = express();
let vtempsreel = [],
    varrets = [],
    vroutes = [];

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

generation();

app.get("/temps-reel/tempsreel", (req, res) => {
    res.json(vtempsreel);
});

app.get("/temps-reel/arrets", (req, res) => {
    res.json(varrets);
});

app.get("/temps-reel/routes", (req, res) => {
    res.json(vroutes);
});

app.listen(8080);